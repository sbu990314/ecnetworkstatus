//
//  ViewController.m
//  Demo
//
//  Created by SU BO-YU on 2013/12/1.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ViewController.h"
#import "ECNetworkStatus.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.hostnameTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)pressGetStatus:(id)sender
{
    switch ([ECNetworkStatus status])
    {
        case ReachableViaWWAN:
            self.statusLabel.text = @"WWAN";
            break;
            
        case ReachableViaWiFi:
            self.statusLabel.text = @"WIFI";
            break;
            
        default:
            self.statusLabel.text = @"NONE";
            break;
    }
}

- (IBAction)pressConnect:(id)sender
{
    [ECNetworkStatus reachabilityWithHostname:self.hostnameTextField.text isConnected:^(BOOL isConnected)
    {
        if (isConnected)
        {
            self.reachabilityLabel.text = @"正常";
        }
        else
        {
            self.reachabilityLabel.text = @"無法連接";
        }
    }];
}
@end
