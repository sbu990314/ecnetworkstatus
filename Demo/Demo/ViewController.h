//
//  ViewController.h
//  Demo
//
//  Created by SU BO-YU on 2013/12/1.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UITextField *hostnameTextField;
@property (weak, nonatomic) IBOutlet UILabel *reachabilityLabel;

- (IBAction)pressGetStatus:(id)sender;
- (IBAction)pressConnect:(id)sender;
@end
