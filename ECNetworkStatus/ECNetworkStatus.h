//
//  ECNetworkStatus.h
//  ECNetworkStatus
//
//  Created by SU BO-YU on 2013/12/1.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface ECNetworkStatus : NSObject

@property (strong, nonatomic) NSString *baseURL;

/**
 *  @brief  目前手機的網路狀態
 *  @return NotReachable        無網路
 *          ReachableViaWiFi    使用 Wifi
 *          ReachableViaWWAN    使用 3G...
 */
+ (NetworkStatus)status;

+ (void)setBaseURL:(NSString *)baseURL;

/**
 *  @brief  檢查指定的網路位置，目前是否能正常連線
 *  @param  hostname 網址
 */
+ (void)reachabilityWithHostname:(NSString *)hostname isConnected:(void (^)(BOOL isConnected))handler;

+ (void)chectNetworkWithHandler:(void(^)(BOOL isConnected))handler;

+ (BOOL)normalStatusCodeWithHTTPURLResponse:(NSHTTPURLResponse *)urlResponse;
@end
