//
//  ECNetworkStatus.m
//  ECNetworkStatus
//
//  Created by SU BO-YU on 2013/12/1.
//  Copyright (c) 2013年 SU BO-YU. All rights reserved.
//

#import "ECNetworkStatus.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>
#import "Reachability.h"

static ECNetworkStatus *instance;

@implementation ECNetworkStatus

+ (ECNetworkStatus *)shared
{
    if (!instance)
    {
        instance = [[ECNetworkStatus alloc] init];
    }
    
    return instance;
}

+ (NetworkStatus)status;
{
    // 創建零地址，0.0.0.0 的地址表示查詢本機的網路連接狀態
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    // 獲得連接標誌
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    // 如果不脦獲取連接標誌，則不能連接網路，直接返回
    if (didRetrieveFlags)
    {
        // 根據獲得的連接標誌進行判斷
        BOOL isReachable = flags & kSCNetworkFlagsReachable;
        BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
        
        if((isReachable && !needsConnection)==YES)
        {
            // 能夠連接焹路
            if((flags& kSCNetworkReachabilityFlagsIsWWAN)==kSCNetworkReachabilityFlagsIsWWAN)
            {
                // 手機網路
                return ReachableViaWWAN;
            }
            else
            {
                // WIFI
                return ReachableViaWiFi;
            }
        }
    }
    
    return NotReachable;
}

+ (void)setBaseURL:(NSString *)baseURL
{
    [ECNetworkStatus shared].baseURL = baseURL;
}

+ (void)reachabilityWithHostname:(NSString *)hostname isConnected:(void (^)(BOOL isConnected))handler
{
    ECNetworkStatus *networkStatus = [ECNetworkStatus shared];
    // 開啟網路監聽
    [[NSNotificationCenter defaultCenter] addObserver:networkStatus
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    Reachability *reach = [Reachability reachabilityWithHostname:hostname];
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            handler(YES);
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            handler(NO);
        });
    };
    
    // 開始監聽，會啟動一個 run loop
    [reach startNotifier];
}

- (void)reachabilityChanged:(NSNotification *)note
{
    Reachability *reach = [note object];
    [reach stopNotifier];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

+ (void)chectNetworkWithHandler:(void(^)(BOOL isConnected))handler
{
    if([ECNetworkStatus status] == NotReachable)
    {
        handler(NO);
        return;
    }
    
    NSAssert(([ECNetworkStatus shared].baseURL), @"請先初始化 ECNetworkStatus.baseURL");
    
    [ECNetworkStatus reachabilityWithHostname:[ECNetworkStatus shared].baseURL isConnected:^(BOOL isConnected)
     {
         if (isConnected)
         {
             handler(YES);
         }
         else
         {
             handler(NO);
         }
     }];
}


+ (BOOL)normalStatusCodeWithHTTPURLResponse:(NSHTTPURLResponse *)urlResponse
{
    if(urlResponse.statusCode == 200)
        return YES;
    
    if (urlResponse.statusCode == 404)
    {
        //[[NSNotificationCenter defaultCenter] postNotificationName:ShowCancelAlertViewNotification object:@[[NSString stringWithFormat:@"網址錯誤:%@", [[urlResponse URL] relativePath]],@"確認"]];
    }
    else
    {
        //[[NSNotificationCenter defaultCenter] postNotificationName:ShowCancelAlertViewNotification object:@[@"網路異常，請檢查網路狀態",@"確認"]];
    }
    
    return NO;
}

@end
